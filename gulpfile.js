var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var uglify = require("gulp-uglify");
var plumber = require("gulp-plumber");
var jade = require("gulp-jade");
var browserify = require('browserify');
var browserSync = require('browser-sync');
var cmq = require('gulp-combine-media-queries');
var compress = require('gulp-yuicompressor');
var rename = require('gulp-rename');
var gulpLoadPlugins = require('gulp-load-plugins');

const $ = gulpLoadPlugins();

// default
gulp.task('default', ['watch']);

gulp.task("sass", function() {
	gulp.src("src/sass/**/*.scss")
		.pipe(plumber())
		.pipe(sass())
		.pipe(cmq())
		.pipe(autoprefixer())
		.pipe(gulp.dest("site/css"))
		.pipe(compress({
			type: 'css'
		}))
		.pipe($.rename({
			extname: '.min.css'
		}))
		.pipe(gulp.dest("site/css"));
});

gulp.task('cmq', function () {
  gulp.src('site/css/*.css')
    .pipe(cmq({
      log: true
    }))
    .pipe(gulp.dest('./site/css/'));
});

gulp.task("js", function() {
  gulp.src(["src/pre_js/**/*.js"])
	  .pipe(plumber())
		.pipe(uglify())
		.pipe(gulp.dest("site/js"));
});

gulp.task('jade', () => {
  gulp.src( './src/jade/**/*.jade')
    .pipe(plumber())
    .pipe(jade({
      pretty: true,
      files: [{
        expand: true,
        cwd: '<%= path.src %>/jade',
        src: '**/!(_)*.jade',
        dest: '<%= path.dist %>',
        ext: '.html'
      }]
    }))
    .pipe(gulp.dest('./site/'));
    return false;
});

// Static server
gulp.task('browser-sync',  () => {
  browserSync({
    server: {
      baseDir: './',
      index: './index.html'
    }
  });
});

// Reload all Browsers
gulp.task('bs-reload',  () => {
    browserSync.reload();
});


gulp.task('watch', ['sass','cmq','js','jade', 'browser-sync'],  () => {
    gulp.watch([ './src/jade/*.jade'], ['jade','bs-reload']);
		gulp.watch(["src/pre_js/**/*.js","!site/js/**/*.js"],["js"]);
		gulp.watch("src/sass/**/*.scss",['sass','cmq']);
});
