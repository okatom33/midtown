/* ------------------------
  Midtown.js
------------------------ */
var Midtown = function($){

  $('a').addClass('hideCursor');

  // 必要な情報を定義する
  var _window = $(window),
      _header = $('#Header'),
      _wrapper = $('#Wrapper'),
      _context,
      _headerHeight = _header.height(),
      _windowWidth = window.innerWidth,
      _windowHeight = window.innerHeight,
      _lightTrigger = $('.js-lightTrigger'),
      _asterisk = $('.js-asterisk'),
      _asteriskHeight = _asterisk.height(),
      _dpr = window.devicePixelRatio,
      _scrollY = _window.scrollY,
      _rectHeight,
      _mousePosition,
      _canvas,
      _mouseY,
      _mouseFlag,
      _initialize,
      _mouseMove,
      _resizeWindow,
      _drawWindow,
      _updateMousePosition,
      _lightChange,
      _lightOff,
      _lightOn,
      _lightFlag,
      _offsetDraw = 0,

      _init = function(){

        /*--------
         device判定
        -------- */
        if(device.mobile()){
          _mouseFlag = false;
          _lightFlag = true;
          _rectHeight = 26;
          _initialize();
          _resizeWindow();
          _lightChange();

        }else{
          _mouseFlag = true;
          _lightFlag = true;
          _rectHeight = 52;
          _initialize();
          _lightChange();
          _mouseMove();
          _resizeWindow();
        }
      };

      /*--------
       初期設定
      -------- */
      _initialize = function(){

        _canvas = document.getElementById('Window');
        _headerHeight = $('#Header').height();
        if(device.mobile()){
          $('#ContentBack').css('top', _headerHeight);
          // アスタリスクの位置・高さ
          _wrapper.css('padding-bottom', _asteriskHeight);
          console.log(_asteriskHeight);
          // _asterisk.css('padding-bottom', _asteriskHeight);
        }else{
          $('#ContentBack').css('top', _headerHeight);
        }
        _canvas.width = _windowWidth * _dpr;
        _canvas.height = _windowHeight * _dpr;
        _drawWindow(0);

      }

      /*--------
       リサイズ処理
      -------- */
      _resizeWindow = function() {
        _window.on('resize', function(){
          _windowWidth = window.innerWidth;
          if (_windowHeight < window.innerHeight) {
            _windowHeight = window.innerHeight;
          }
          _initialize();
        })
      }

      /*--------
       canvasを描画
      -------- */
      _drawWindow = function(_mouseY) {

        if ( ! _canvas || ! _canvas.getContext ) { return false; }
        _context = _canvas.getContext('2d');

        if (_lightFlag) {

          // lightFlagがtrueの時はcanvasで暗くする
          _context.clearRect(0, 0, _windowWidth, _windowHeight);

          // 全面を黒く塗りつぶす
          _context.fillStyle = 'rgba(20,20,20,1)';
          _context.fillRect(0,0, _windowWidth, _windowHeight);

          if(device.mobile()){
            // 画面中央に光
            var drawY = _windowHeight / 2 - 80;
            _context.clearRect(0, drawY + _offsetDraw, _windowWidth, _rectHeight);
          }else{
            var drawY = _mouseY - (_rectHeight / 2);
            _context.clearRect(0, drawY, _windowWidth, _rectHeight);
          }
        }
      }

      /*--------
       mouseイベントの処理
      -------- */
      _mouseMove = function() {
        $(document).on('mousemove', function(e){
          _mousePosition = e.clientY;
          _updateMousePosition();
          _drawWindow(_mousePosition);
        });
      }

      /*--------
       mouseイベントの処理
      -------- */
      _updateMousePosition = function() {
        var pos = _scrollY + _mousePosition - (_rectHeight / 2);
        _mouseFlag = !(pos > _headerHeight);
      }

      /*--------
       電気を消す/つける処理
      -------- */
      _lightChange = function(){
        _lightTrigger.on('click', function(){
          if($(this).hasClass('js-lightTrigger-off')){
            _lightOn();
            _initialize();
          }else{
            _lightOff();
            _initialize();
          }
        });
      }

      /*--------
       電気をつける = canvasを透明にする
      -------- */
      _lightOn = function(){
        _lightFlag = false;
        _lightTrigger.text('※電気を消す');
        _lightTrigger.removeClass('js-lightTrigger-off');
        if(device.mobile()){
          _wrapper.css('background', 'rgba(20,20,20,0)');
        }else{
          _context.fillStyle = 'rgba(20,20,20,0)';
          _asterisk.css('padding-bottom', '');
          $('.Content').addClass('Content--pointer');
          $('.Content__text').addClass('Content__text--pointer');
          $('.Content__link').addClass('Content__link--pointer');
          $('.Content__image').addClass('Content__image--pointer');
        }

        $('body').removeClass('hideCursor');
        $('a').removeClass('hideCursor');
      }

      /*--------
       電気を消す = canvasを暗くする
      -------- */
      _lightOff = function(){
        _lightFlag = true;
        _lightTrigger.text('※電気をつける');
        _lightTrigger.addClass('js-lightTrigger-off');
        if(device.mobile()){
          _wrapper.css('background', 'rgba(20,20,20,1)');
        }else{
          _context.clearRect(0, 0, _windowWidth, _windowHeight);
          _context.fillStyle = 'rgba(20,20,20,1)';
          _context.fillRect(0,0, _windowWidth, _windowHeight);
          $('.Content').removeClass('Content--pointer');
          $('.Content__text').removeClass('Content__text--pointer');
          $('.Content__link').removeClass('Content__link--pointer');
          $('.Content__image').removeClass('Content__image--pointer');
        }

        $('body').addClass('hideCursor');
        $('a').addClass('hideCursor');
      }

  return _init;

}(jQuery);

// Midtownを実行
Midtown();
